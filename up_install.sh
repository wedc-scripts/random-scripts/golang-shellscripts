#!/usr/bin/env bash
# -*- coding: utf-8 -*-
set -e
# ==============================================================================
# title             : Semi Automatic Update GoLang
# description       : Script for Install, setup path, permission and update golang
# author            : Walddys Emmanuel Dorrejo Céspedes
# usage             : bash up_install.sh
# notes             : Execute this Script will ask sudo password
# dependencies      : wget awk sed curl tar
# ==============================================================================

## Get Golang Versions from repository
declare -a go_versions
readarray -t go_versions < <(curl -s https://go.googlesource.com/go/+refs?format=JSON | grep -Po "go[[:digit:]].*[[:digit:]]" | sort -V)

## Get the System GoLang Env
if [ "$(command -v go)" ]; then
    GOROOT=$(go env GOROOT)
    V_LOCAL=$(go version | sed -e 's/[a-zA-Z\ ]//g; s/\/.*//') # Version Installed
else
    GOROOT=/usr/local/go
    if [ ! -d "$GOROOT" ]; then
        sudo mkdir -p "$GOROOT" || (echo "Can't create directory where go will be placed" && exit 40)
    fi
fi

## Show the release candidate
if (echo "${go_versions[-1]}" | grep -qi "rc"); then
    echo -e "\tLatest version found in the repository is: ${go_versions[-1]}."
    echo -e "\tIs a release candidate and is not recommended for production or learn."
    ## Loop prompt
    while :; do
        read -t 30 -r -p "Do you which to install this version? (y/N) " item
        item=N
        case "${item}" in
        Y | y) ## If yes install the release candidate
            break
            ;;
        N | n) ## if not will remove all release candidate and then install the latest stable version.
            clear
            for i in "${!go_versions[@]}"; do
                ## Delete go_versions release candidate from the pool.
                if echo "${go_versions[i]}" | grep -q "$(echo "${go_versions[-1]}" | grep -Eo "[0-9]*\.[0-9]+")"; then
                    unset "go_versions[$i]"
                fi
            done
            break
            ;;
        *)
            echo "Please select Y or N"
            sleep 2s
            clear
            ;;
        esac
    done
fi

# Install go for the first time
if [ ! "$(command -v go)" ]; then
    clear
    echo 'First Time Go Setup'
    echo -e "\tDirectory where is recommend to be install: /usr/local/go"
    echo -e "\tLatest Stable Version that will be installed is: ${go_versions[-1]}"
    sudo sh -c 'wget -q --show-progress https://go.dev/dl/'"${go_versions[-1]}"'.linux-amd64.tar.gz -O - | tar xzf - --strip-components=1 -C '"${GOROOT}"'/'
    sudo sh -c 'chown -R $(id -nu "$SUDO_USER"):$(id -ng "$SUDO_USER") '"$GOROOT"
elif [ "$(command -v go)" ]; then
    clear
    echo -e "Golang Upgrading Protocol Activate"
    echo -e "\tCurrent GoLang Env:"
    echo -e "\t- Golang Version: $(go version)"
    echo -e "\t- Golang GOPATH: $(go env GOPATH)"
    echo -e "\t- Golang GOROOT: $(go env GOROOT)"
    echo -e "\n\n"

    if [[ "${V_LOCAL}" < "${go_versions[-1]}" ]]; then
        sudo rm -rf "${GOROOT}"
        sudo sh -c 'wget -q --show-progress https://go.dev/dl/'"${go_versions[-1]}"'.linux-amd64.tar.gz -O - | tar xzf - --strip-components=1 -C '"${GOROOT}"'/'
        sudo sh -c 'chown -R $(id -nu "$SUDO_USER"):$(id -ng "$SUDO_USER") '"${GOROOT}"
    else
        echo "Don't need update"
    fi
fi

case "${SHELL}" in
/bin/bash)
    if ! grep -qE "${GOROOT}/bin" "$HOME"/.bashrc; then
        # awk '/^export PATH/ {$0=$0":'${{GOROOT}}'"} 1' > .bashrc # AWK way to append go path to the existing user "export PATH"
        sed -i.bk "s_^export PATH.*_&:${GOROOT}/bin_" "$HOME"/.bashrc # SED way to append go path to the existing user "export PATH"
        source "$HOME"/.bashrc
    fi
    ;;
/bin/zsh)
    if ! grep -E "${GOROOT}/bin" "$HOME"/.zshrc; then
        # awk '/^export PATH/ {$0=$0":'${{GOROOT}}'"} 1' > .bashrc # AWK way to append go path to the existing user "export PATH"
        sed -i.bk "s_^export PATH.*_&:${GOROOT}/bin_" "$HOME"/.zshrc # SED way to append go path to the existing user "export PATH"
        source "$HOME"/.zshrc
    fi
    ;;
*)
    echo "Don't know you shell, please create a issue or pull request, to add the shell you are using. (https://gitlab.com/wedc-scripts/random-scripts/golang-shellscripts)"
    ;;
esac
